package com.example.springproject.controller;

import com.example.springproject.model.User;
import com.example.springproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    UserService userService;


    @PostMapping("/create")

    User create(@RequestBody User user){
        return userService.save(user);
    }


    @GetMapping("/users")

    Iterable <User>read(){
        return userService.findAll();
    }

    @PutMapping("/user")
    User update(@RequestBody User user){
        return userService.save(user);
    }

    @DeleteMapping("/user/{id}")
    void delete(@PathVariable Integer id){
        userService.deleteById(id);
    }

}


