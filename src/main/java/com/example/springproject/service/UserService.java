package com.example.springproject.service;

import com.example.springproject.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserService extends CrudRepository<User, Integer> {

   // List<User> read();
     //void create(User user);
     //User find(int id);
    // void delete(User user);
    // void update(User user);
}
