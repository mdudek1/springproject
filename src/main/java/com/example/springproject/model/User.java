package com.example.springproject.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User {

    @Id
    @GeneratedValue (strategy= GenerationType.IDENTITY)
    private int ID;
    private String name;
    private double setHour;
    private int setTemperature;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSetHour() {
        return setHour;
    }

    public void setSetHour(double setHour) {
        this.setHour = setHour;
    }

    public int getSetTemperature() {
        return setTemperature;
    }

    public void setSetTemperature(int setTemperature) {
        this.setTemperature = setTemperature;
    }
}
